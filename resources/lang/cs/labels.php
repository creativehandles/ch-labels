<?php

return [
    'Create Label' => 'Vytvořit štítek',
    'id' => 'id',
    'Label' => 'Název',
    'Color' => 'Barva',
    'Plugin' => 'Plugin',
    'Update' => 'Upravit',
    'Delete' => 'Smazat',

    'Update Label' => 'Upravit štítek',
    'Create new label' => 'Vytvořit nový štítek',
    'Name' => 'Název',
    'Color' => 'Barva',
    'Target' => 'Cíl',
    'select a plugin' => 'vyberte plugin',

    '404' => 'Štítek nebyl nalezen.'
];
