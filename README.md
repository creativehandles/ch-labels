# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/creativehandles/ch-labels.svg?style=flat-square)](https://packagist.org/packages/creativehandles/ch-labels)
[![Build Status](https://img.shields.io/travis/creativehandles/ch-labels/master.svg?style=flat-square)](https://travis-ci.org/creativehandles/ch-labels)
[![Quality Score](https://img.shields.io/scrutinizer/g/creativehandles/ch-labels.svg?style=flat-square)](https://scrutinizer-ci.com/g/creativehandles/ch-labels)
[![Total Downloads](https://img.shields.io/packagist/dt/creativehandles/ch-labels.svg?style=flat-square)](https://packagist.org/packages/creativehandles/ch-labels)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require creativehandles/ch-labels
```

## Usage

Once you install the package via composer, you need to follow these things in order to make this package work within the CMS repository.

1.Run `php artisan creativehandles:build-label-plugin` to generate all required stuff

2.Finally run `php artisan cms:populate-permissions` command to populate permission tables.

Thats all. You can go to `cms.test/admin/labels` and check new label plugin you have installed

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email deemantha@creativehandles.com instead of using the issue tracker.

## Credits

- [Deemantha Kasun](https://github.com/creativehandles)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).