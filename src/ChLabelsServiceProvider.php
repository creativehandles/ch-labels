<?php

namespace Creativehandles\ChLabels;

use Creativehandles\ChLabels\Console\BuildLabelPackageCommand;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;

class ChLabelsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
//         $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'ch-labels');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'ch-labels');
         $this->loadMigrationsFrom(__DIR__.'/Plugins/Labels/Migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {

            //publish config files
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('ch-labels.php'),
                __DIR__.'/../config/labelModelMapping.php' => config_path('labelModelMapping.php'),
            ], 'config');

            // Publishing the controllers.
            $this->publishes([
                __DIR__.'/../app/Http/Controllers/PluginsControllers' => app_path('/Http/Controllers/PluginsControllers'),
            ], 'pluginController');

            // Publishing the views.
            $this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/Admin/'),
            ], 'views');

            //publishing routes and breadcrumbs
            $this->publishes([
                __DIR__.'/../routes' => base_path('routes/packages'),
            ], 'views');

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/ch-labels'),
            ], 'assets');*/

            // Publishing the translation files.
            $this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/'),
            ], 'lang');

            // Registering package commands.
            $this->commands([
                BuildLabelPackageCommand::class,
            ]);
        }

    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'ch-labels');

        // Register the main class to use with the facade
        $this->app->singleton('ch-labels', function () {
            return new ChLabels;
        });
    }
}
