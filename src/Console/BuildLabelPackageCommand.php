<?php

namespace Creativehandles\ChLabels\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class BuildLabelPackageCommand extends Command
{
    protected $signature = 'creativehandles:build-label-plugin';

    protected $description = 'Build all prerequisites to start the plugin';

    public function handle()
    {
        $this->info('Publising vendor directories');
        $this->callSilent('vendor:publish', ['--provider' => 'Creativehandles\ChLabels\ChLabelsServiceProvider']);

        $this->info('Migrating tables');
        //run migrations in the plugin
        Artisan::call('migrate --path=app/Plugins/Labels/Migrations');

        $this->info('Migration complete');

        //seed active plugins table
        if (! DB::table('active_plugins')->where('plugin', 'Labels')->first()) {
            DB::table('active_plugins')->insert([
                'plugin'=>'Labels',
            ]);
        }

        $this->info('Good to go!!');
    }
}
