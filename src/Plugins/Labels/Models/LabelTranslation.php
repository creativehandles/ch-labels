<?php

namespace Creativehandles\ChLabels\Plugins\Labels\Models;

use Illuminate\Database\Eloquent\Model;

class LabelTranslation extends Model
{
    protected $table = 'label_translations';
    public $fillable = ['label', 'color', 'author'];
}
