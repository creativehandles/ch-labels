<?php

namespace Creativehandles\ChLabels\Plugins\Labels\Models;

use Illuminate\Database\Eloquent\Model;

class LabelRelation extends Model
{
    public function label()
    {
        return $this->belongsTo("App\Plugins\Labels\Models\Label", 'label_id')->orderBy('id', 'asc');
    }

    protected $table = 'labels_relation';
    protected $guarded = [];
}
