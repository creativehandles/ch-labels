<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 13/8/19
 * Time: 6:22 PM.
 */

namespace Creativehandles\ChLabels\Plugins\Labels\Repositories;

use App\Repositories\Exceptions\EloquentRepositoryException;
use Creativehandles\ChLabels\Plugins\Labels\Models\Label;
use App\Repositories\BaseEloquentRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use function request;

class LabelRepository extends BaseEloquentRepository
{
    /**
     * Get the model to be used for the repository;
     *
     * @return Label
     */
    public function getModel()
    {
        return new Label();
    }

    /**
     * Updates a label or create a new one
     *
     * @param array $attributes With either ['id'] or ['model', 'locale', 'label'] keys
     * @param array $values
     * @return Model
     * @throws EloquentRepositoryException
     */
    public function updateOrCreateByMultipleKeys(array $attributes, array $values = [])
    {
        if (! empty($attributes['id'])) {
            $label = $this->model->firstOrNew(['id' => $attributes['id']]);
        } else {
            $label = $this->model->where('model', '=', Arr::get($attributes, 'model'))->whereTranslation('label', $attributes['label'], $attributes['locale'])->first();

            if ($label === null) {
                $label = $this->getModel();
            }
        }

        $label->model = $values['model'];
        $labelTranslatable = $label->translateOrNew($values['locale']);
        $labelTranslatable->label = $values['label'];
        $labelTranslatable->color = $values['color'];
        $labelTranslatable->author = $values['author'];
        $label->save();

        return $label;
    }

    /**
     * Returns a data array needed for JS DataTables plugin
     *
     * @param array $searchColumns Columns to use for searching
     * @param array $orderColumns Array of columns to order
     * @param string $defOrderColumnKey Key for a column in $orderColumns array to be used as fallback column to order
     * @param array $with Array of Eloquent relationships to eager load.
     * @param array $where where filter ['column,'condition','value']
     *
     * @return array
     * @throws EloquentRepositoryException
     */
    public function getDataforDataTables(array $searchColumns = null, array $orderColumns = null, string $defOrderColumnKey = null, array $with = null, array $where= null)
    {
        $orderColumn = null;
        $dir = request()->input('order.0.dir', 'asc');

        $this->model = $this->model->selectRaw('labels.*, label_translations.id as label_translation_id, label_translations.locale, label_translations.label, label_translations.color, label_translations.author, label_translations.created_at, label_translations.updated_at');

        if ($orderColumns !== null && $defOrderColumnKey !== null) {
            $orderColumn = $orderColumns[request()->input('order.0.column')] ?? $orderColumns[$defOrderColumnKey];
        }

        $this->model = $this->model->join('label_translations', 'labels.id', '=', 'label_translations.label_id');

        if ($orderColumn !== null) {
            $this->model = $this->model->orderBy($orderColumn, $dir)
                ->orderBy($orderColumns[$defOrderColumnKey], 'asc');
        }

        return parent::getDataforDataTables($searchColumns, null, null, $with, $where);
    }

    /**
     * Build the data index of the return array for the JS DataTables plugin
     *
     * @param Collection|array $collection
     * @return array
     */
    public function getDataIndexforDataTables($collection)
    {
        $data = [];

        foreach ($collection as $item) {
            $data[$item->id] = [
                'id' => $item->id,
                'label' => $item->label,
                'color' => $item->color,
                'model' => $item->model
            ];
        }

        return array_values($data);
    }
}
