<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferDataToLabelTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // move the existing data into the translations table
        DB::statement(
            'insert into label_translations (label_id, locale, label, color, author) select id, :locale, label, color, author from labels',
            ['locale' => config('app.locale')])
        ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Move the translated data back into the main table. Translations of the default language of the application are selected.
        DB::statement(
            'update labels as m join label_translations as t on m.id = t.label_id set m.label = t.label, m.color = t.color, m.author = t.author where t.locale = :locale',
            ['locale' => config('app.locale')])
        ;
    }
}
