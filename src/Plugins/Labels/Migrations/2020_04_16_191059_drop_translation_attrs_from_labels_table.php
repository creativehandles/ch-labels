<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTranslationAttrsFromLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // we drop the translation attributes in our main table:
        Schema::table('labels', function ($table) {
            $table->dropColumn('label');
            $table->dropColumn('color');
            $table->dropColumn('author');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // add the translatable attributes back in to main table
        Schema::table('labels', function ($table) {
            $table->string('label', 191)->after('id');
            $table->string('color', 191)->after('label');
            $table->unsignedInteger('author')->after('model');
        });
    }
}
