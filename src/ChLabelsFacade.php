<?php

namespace Creativehandles\ChLabels;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\ChLabels\Skeleton\SkeletonClass
 */
class ChLabelsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ch-labels';
    }
}
