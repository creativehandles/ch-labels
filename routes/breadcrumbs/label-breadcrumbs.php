<?php

/** Labels BreadCrumbs */
// Dashboard > labels
Breadcrumbs::for('admin.labels', function ($trail) {
    $trail->parent('dashboard');
    $trail->push(__('general.Labels'), route('admin.labels'));
});

// Dashboard > labels > create
Breadcrumbs::for('admin.labels.create', function ($trail) {
    $trail->parent('admin.labels');
    $trail->push(__('general.Create Label'), route('admin.labels.create'));
});

// Dashboard > labels > edit
Breadcrumbs::for('admin.labels.edit', function ($trail, $label) {
    $trail->parent('admin.labels');
    $trail->push(__('general.Edit Label'), route('admin.labels.edit', $label->label ?? ''));
});
