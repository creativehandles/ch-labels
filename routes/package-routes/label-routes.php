<?php

//label related
Route::group(['name' => 'labels', 'groupName' => 'labels'], function () {
    //labels related routes
    Route::get('/labels/grid', 'PluginsControllers\LabelsController@grid')->name('labelsGrid');
    Route::resource('/labels', 'PluginsControllers\LabelsController', [
        'names' => [
            'index' => 'labels',
        ],
    ]);
});
